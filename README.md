## Backgammon - AI and Strategy
#### Author: Marcus Fong | Date last edited: 29th May 2018
In this project, I created an AI to play against a random AI in the game of
Backgammon in the Haskell script "PlayAsWhite.hs". The AI uses the min-max
algorithm and utilises a heuristic function for decision making.




