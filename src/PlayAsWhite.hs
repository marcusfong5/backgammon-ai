-- Backgammon AI
-- Author: Marcus Fong | Date: 29th May 2018

module PlayAsWhite where

import State
import Data.List
import Board
import Data.Tree
import Move
import Debug.Trace

makeMove :: State -> Lookahead -> Moves
makeMove s _ = concat (replicate 4 (greedyBot s))
--                 | length (movesLeft s) == 4 || wBar (board s) > 0 = (concat (replicate 4 [(x, y) | x <- [0 .. 24], y <- [1 ..6]]))
--                 | otherwise = greedyBot s


-- Initial frameworks for very simple bots:
-- MakelegalMove bot: takes the move(s) that gets checkers closest to position 0 (finish line) on Board
-- ie: For any move (p, n), take move with smallest p and largest n
-- step 1: list all legal moves
-- step 2: have function that selects move with smallest p and largest n
-- step 3: send move to makeMove function

-- PipCount Bot: takes the move(s) that lowest pip count the most
-- A function that finds legal move with the lowest wPips count and selects it in makeMove
-- step 1: Have function1 that lists all legal moves (not sure how to do)
-- step 2: Have function2 that labels each legal move with wPip count
-- step 3: Have function3 that selects labelled legal move with lowest wPips count

-- function that quantifies how good a state is for the bot
heuristic :: State -> Int
heuristic s = (function s) + (function2 s) + (function3 s)
  where
-- function: variable that is difference between the two player's pips
    function :: State -> Int
    function s = (bPips s) - (wPips s)
-- function2: variable that evaluates how much is weighted to each new black pip on bar
    function2 :: State -> Int
    function2 s
      | bBar (board s) == 0 = 0
      | bBar (board s) == 1 = 40
      | bBar (board s) == 2 = 50
      | bBar (board s) >= 3 = 60
      | otherwise = 1
-- function3: variable that evaluates how much is weighted to each new white pip on bar
    function3 :: State -> Int
    function3 s
      | wBar (board s) == 0 = 0
      | wBar (board s) == 1 = -20
      | wBar (board s) == 2 = -25
      | wBar (board s) >= 3 = -30
      | otherwise = -1

-- function that correctly labels each move with its heuristic
-- assigns heuristic after performing move to the move
label :: State -> Move -> (Int, Int, Int)
label s (x, y)= ((heuristic ( performSingleMove s (x, y))), x, y)

-- function that labels all legal moves for given state with heuristic
-- sort and reverse reorders best move at the front of list and worst at back
labelLegalMoves :: State -> [(Int, Int, Int)]
labelLegalMoves s = reverse ( sort ( map (label s) (legalMoves s)))

-- function that removes heuristic "label" on the moves list
listHeuristicToMoves :: [(Int, Int, Int)] -> Moves
listHeuristicToMoves x = map heuristicToMove x
  where
  heuristicToMove (_,y,z) = (y,z)

-- A list of moves which will be used in the makeMove function
greedyBot :: State -> Moves
greedyBot s = listHeuristicToMoves (labelLegalMoves s) ++ concat (replicate 4 [(x, y) | x <- [0 .. 24], y <- [1 ..6]])


-- MiniMax attempt

possibleStatesTree' :: Lookahead -> State -> Tree State
possibleStatesTree' l state = possibleStatesTree l (legalMovesTree state)

possibleStatesTree :: Lookahead -> Tree State -> Tree State
possibleStatesTree l state
  | l == 0 = state
  | otherwise = case state of
      Node _ []         -> state
      Node state' list' -> Node state' (map (possibleStatesTree (l-1)) list')

stateIsEmpty :: State -> Bool
stateIsEmpty state = case movesLeft state of
  [] -> True
  _ -> False

legalMovesTree' :: State -> Tree State
legalMovesTree' =
  unfoldTree f
    where
      f :: State -> (State, [State])
      f s = (s, map (performSingleMove s) (legalMoves s))

treeMap :: (a -> b) -> Tree a -> Tree b
treeMap function tree = case tree of
  Node a [] -> Node (function a) []
  Node a list -> Node (function a) (map (treeMap function) list)



